# Disciplina de Introdução à Ciência de Dados - UTFPR



## Professores

- Luiz Celso Gomes Jr (UTFPR) - [Página pessoal](http://dainf.ct.utfpr.edu.br/~gomesjr/) - [Twitter](https://twitter.com/luizcelso)


## Objetivos

Ensinar os principais conceitos de Ciência de Dados com aplicação prática em problemas atuais. Uma ênfase especial será dada em:



*   Complexidade do domínio e importância de inferências bem fundamentadas
*   Multidisciplinaridade
*   Diversidade de modelos




## Conteúdo programático


*   Introdução e Contextualização: Motivação, exemplos, história, contexto e visão geral de tecnologias relacionadas.
*   Obtenção e Análise de Dados: Técnicas para a obtenção, limpeza e processamento de dados.
*   Tópicos em modelagem estatística: conceitos básicos, projeto de experimentos e interpretação de resultados.
*   Visualização de Dados: Principais técnicas para a apresentação visual de dados e resultados. 
*   Tópicos em aprendizado de máquina: aprendizado supervisionado e aprendizado não supervisionado.
*   Manipulação de Dados em Grande Escala: Limitações do Modelo Relacional, Bancos de Dados Paralelos e Distribuídos, Bancos NoSQL, MapReduce, Modelos de Armazenamento e Linguagens de Processamento Distribuído
*   Modelagem de alta dimensionalidade: Ciência de Redes, Propriedades das Redes Complexas, Tópicos em processamento de linguagem natural
*   Ética em Ciência de Dados



## Modalidade, Metodologia e Infraestrutura

### Aulas

As aulas se desenvolverão no ambiente virtual Google Classroom de forma assíncrona, mas alunos terão a opção de participar de conferências de debate e atendimento online.



*   Vídeos serão postados online no início de cada semana.
*   Sessões para debates sobre o tema da semana no horário de aula (programação divulgada no início do curso).

Também haverá oportunidade de se tirar dúvidas e participar de debates de forma assíncrona no Discord.


### Google Classroom

Avisos, notas de aula, conteúdos de leitura suplementar, listas de exercício, resultados de avaliações e todas as atividades de participação serão compartilhadas no ambiente Google Classroom. 



### Atividades Avaliativas

Em geral a avaliação é baseada em um trabalho de análise feito em grupo ao longo do período. Pequenas variações podem ocorrer dependendo do período, mas sempre comunicadas no início do curso.



## Perguntas Frequentes

### Não sou aluno de computação. Posso fazer a disciplina?

Sim! Multidisciplinaridade é muito importante em Ciência de Dados

### É preciso experiência em programação?

Não é preciso, mas é importante se familiarizar com os conceitos e aprender os procedimentos mais comuns.


## Recursos úteis

### Material didático

- [Tutoriais de Python e Pandas](https://gitlab.com/introcienciadedados/tutoriais)

### Projetos relacionados

Projetos com participação dos professores da disciplina. Podem ser usados como fontes de inspiração e dados.

- [Ciência de Dados por uma Causa](https://dadosecausas.org/)
- [COVID-19 Como está meu país?](https://coronavirus.dadosecausas.org/)
- [Observatório COVID-19 BR](https://covid19br.github.io/)

### Fontes de dados

Algumas fontes recomendadas de dados.

- [Brasil.io](https://brasil.io/dataset/covid19/caso)
- [Our World in Data](https://ourworldindata.org/)
- [The World Bank](http://datatopics.worldbank.org/universal-health-coverage/covid19/)
- [PNAD-COVID19 IBGE](https://covid19.ibge.gov.br/pnad-covid/)

### Ferramentas

Principais ferramentas discutidas na disciplina (alunos são encorajados a explorar ferramentas complementares se puderem).

- [pandas (python) - Biblioteca de manipulação de dados](https://pandas.pydata.org/)
- [Jupyter (python) - Ambiente de desenvolvimento e documentação](https://jupyter.org)
- [RStudio (R) - Ambiente de desenvolvimento](https://rstudio.com/)
- [Git - Controle de versão](https://git-scm.com/)
- [SmartGit - Interface multiplataforma para o Git](https://www.syntevo.com/smartgit/download/)

## Ofertas anteriores

- [2020/2](https://introcienciadedados.gitlab.io/)


